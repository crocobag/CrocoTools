# CrocoBag

[![Swift Package Manager](https://img.shields.io/badge/Swift_Package_Manager-compatible-green?style=flat-square)](https://img.shields.io/badge/Swift_Package_Manager-compatible-green?style=flat-square)

The Swift CrocoBag is a library (soon to be) filled with Helpers written in Swift.

- [Features](#features)
    - [Networking](#Networking)
    - [Widgets](#Widgets)
- [Installation](#installation)


## Features

### Networking

Inspiration from [Víctor Pimentel](https://medium.com/@VictorPimentel) and his tutorial on [Writing a Scalable API Client in Swift 5](https://medium.com/makingtuenti/writing-a-scalable-api-client-in-swift-4-b3c6f7f3f3fb)

This method helps you handle calls made to an API very easily. It is very straightforward to add new calls, endpoints and test them. 

*Usage*

This example hits the token free [catfacts API](https://catfact.ninja) made by an unknown hero (who can email me if he or she finds this package)

**A simple get request via JSON**
```
/// 1. Subclass ApiClient 
class CatApiClient: ApiClient {
    var baseUrl = URL(string: "https://catfact.ninja")!
    
    // Singletoned
    static let shared = CatApiClient()
    private init() {}
}

/// 2. Create the decodable structure that maches the JSON response 
/// {
///   "fact": "string",
///   "length": 0
/// }
/// 
struct CatFact: Codable {
    var fact: String
    var length: Int
}

// 3. Subclass ApiRequest
class CatRequests {
    
    class GetFact: ApiRequest {
        var route = "fact"
            
        var method = HTTPMethod.get
            
        func process(data: Data?, response: URLResponse?) -> CatFact? {
            return (data != nil) ? try? JSONDecoder().decode(CatFact.self, from: data!) : nil
        }
    }
}

// 4. Call the API 
CatApiClient.shared.send(CatRequests.GetFact()) { fact in 
    print(fact) // fact is of type `CatFact` inferred by `GeFact` definition
}
```

*TODO* 

- [ ] add test coverage
- [ ] add HTTP codes
- [ ] add ApiLogger *to handle HTTP codes*

- [ ] Change `process` method so that it be implemented by default in a protocol extension, returning the statuscode 

### Widgets 

#### NibView 

Eventhough this type of structure has been highlighted times and times before, I still want to thank my former coworker Clément C., who will recognize himself, for showing me this helper for the first time. I didn't even try to change it's naming, it's accurate and straight to the point. 

`NibView` is an `UIView`'s subclass that is meant to load an UIView from a xib file (for reusability purposes). 

```
    // The subclass
    @IBDesignable MyView: NibView {
        ...
    }
 
    // Can be registered as an outlet
    @IBOutlet weak var myView: MyView! // from IB

    // or programmatically
    let myView = MyView() // Programmatically
```

_Note: By adding `@IBDesignable` before keyword, it allows the preview to be displayed when added to the storyboard as an outlet. 
~~However this feature currently doesn't work properly on M1 chip~~ IBDesignables are now fixed in XCode 13. All `NibView`'s subclasses can now be seen on storyboard by M1 users_ 

## Installation

### Swift Package Manager

The [Swift Package Manager](https://swift.org/package-manager/) Apple's tool for automating the distribution of Swift code and is integrated into the `swift` compiler.

```swift
dependencies: [
    .package(url: "https://github.com/Olympiloutre/Crocobag.git", .upToNextMajor(from: "0.0.1"))
]
```


