//
//  NibView.swift
//  Libraries Project
//
//  Created by Romuald Brochard on 19/09/2021.
//

import UIKit

/**
 Helper Class to load UIView's from xib files
  
 How to :
  
    1. creates a subclass of NibView and add outlets and dedicated methods
    2. create a view .xib file with the same naming
    3. set its File Owner with the class name
    3. add outlets to the xib and connect them to the subclass
 
    - Warning:  both .swift and .xib files must share the same name
 
 
 # Example #
 
    ```
    // The subclass
    @IBDesignable MyView: NibView {
        ...
    }
 
    // In the View Controller
    @IBOutlet weak var myView: MyView! // from IB

    let myView = MyView() // Programmatically
    ```
 
 # Discussion #
    Preview as IBOutlet in interface builder is supported on M1 chipsets since XCode 13
*/
open class NibView: UIView {
    
    public var nibName: String {
        get {
            String(describing: type(of: self))
        }
    }
    
    public var contentView: UIView?
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.nibName, bundle: bundle)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return
        }
        
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            self.topAnchor.constraint(equalTo: view.topAnchor)
        ])
        
        contentView = view
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadViewFromNib()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadViewFromNib()
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.loadViewFromNib()
        contentView?.prepareForInterfaceBuilder()
    }
}
