//
//  File.swift
//  
//
//  Created by Romuald Brochard on 22/09/2021.
//

import Foundation
import OSLog

public protocol ApiClient: AnyObject {
    var baseUrl: URL { get }
    var commonHeaders: [String: String] { get }
    var logger: Logger? { get }

    func send<T: ApiRequest>(_ request: T, completion: @escaping (T.CallBackType) -> Void)
}

public extension ApiClient {
    var logger: Logger? { Logger() }
    var commonHeaders: [String: String] { [:] }

    func endpoint<T: ApiRequest>(for request: T) -> URL? {
        guard let url = URL(string: request.route, relativeTo: baseUrl),
              var components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            self.logger?.error("Invalid URL built for route \(request.route) base url \(self.baseUrl)")
            return nil
        }

        components.queryItems = request.queryItems.map({ key, value in
            URLQueryItem(name: key, value: value)
        })

        return components.url!
    }
    
    func send<T: ApiRequest>(_ request: T, completion: @escaping (T.CallBackType) -> Void) {
        guard let endpoint = self.endpoint(for: request) else {
            return
        }
        
        var task = URLRequest(url: endpoint)
        
        var headers = self.commonHeaders.merging(request.headers, uniquingKeysWith: { (_, new) in new })

        if let json = request.json {
            var jsonData: Data!
            do {
                jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
            } catch {
                self.logger?.error("\(error.localizedDescription)")
            }

            task.httpBody = jsonData
            headers["application/json"] = "Content-Type"
        }

        headers.forEach { key, value in
            task.setValue(value, forHTTPHeaderField: key)
        }
        
        task.httpMethod = request.method.rawValue

        let dataTask = URLSession.shared.dataTask(with: task, completionHandler: { [weak self] data, response, error in
            if (error != nil) {
                self?.logger?.error("\(error!.localizedDescription)")
                return
            }

            if let httpResponse = response as? HTTPURLResponse {
                let statusCode = httpResponse.statusCode;
                switch statusCode {
                case 100 ..< 300:
                    self?.logger?.debug("[\(statusCode)] - \(request.method.rawValue) \(request.route)")
                default:
                    self?.logger?.warning("[\(statusCode)] - \(request.method.rawValue) \(request.route)")
                }
            } else {
                self?.logger?.warning("[UNKNOWN STATUSCODE] - \(request.method.rawValue) \(request.route)")
            }

            let callback = request.process(data: data, response: response)
            
            completion(callback)
        })
        
        dataTask.resume()
    }
}
