//
//  File.swift
//  
//
//  Created by Romuald Brochard on 21/09/2021.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case head = "HEAD"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case connect = "CONNECT"
    case options = "OPTIONS"
    case trace = "TRACE"
    case patch = "PATCH"
}

typealias HTTPStatusCode = Int

public protocol ApiRequest {
    associatedtype CallBackType

    var route: String { get }
    var headers: [String: String] { get }
    var queryItems: [String: String] { get }
    var json: [String: Any]? { get }
    var method: HTTPMethod { get }

    func process(data: Data?, response: URLResponse?) -> CallBackType
}

public extension ApiRequest {
    var headers: [String: String] { [:] }
    var queryItems: [String: String] { [:] }
    var json: [String: Any]? { nil }
}
